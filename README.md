###这是什么

这个项目用于百姓网的类目自动推荐，是一个正在进行中的项目。

###文件结构

    --| data 分类器的模型
        --| title_clf.pkl
        ...

    --| gearman gearman的worker和client
        --| ml_worker.py: worker
        --| ml_client.py: python client
        --| ml_client.php: php client 

    --| namemap.json: 二级类目中英文名对应

    --| parentmap.json: 二级类目与一级类目对应

###如何启动一个worker

    $ cd gearman
    $ python ml_worker.py
