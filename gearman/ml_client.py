# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net
自动推荐类目的python client
"""

import gearman
import json

def check_request_status(job_request):
    if job_request.complete:
        print "Job %s finished!  State: %s" % (job_request.job.unique, job_request.state)
        result = json.loads(job_request.result)
        for item in result:
            print "%s > %s: %s" % (item[0], item[1], item[2])
    elif job_request.timed_out:
        print "Job %s timed out!" % job_request.unique
    elif job_request.state == gearman.JOB_UNKNOWN:
        print "Job %s connection failed!" % job_request.unique

gm_client = gearman.GearmanClient(['192.168.1.40'])
print "start create job"
completed_job_request = gm_client.submit_job("category_learning", "现有大批量高精仿手表对外转让 支持货到付款 验货满")
print "finish job"
check_request_status(completed_job_request)
